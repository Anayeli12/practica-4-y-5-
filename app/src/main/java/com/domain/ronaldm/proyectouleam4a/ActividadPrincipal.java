package com.domain.ronaldm.proyectouleam4a;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class ActividadPrincipal extends AppCompatActivity implements View.OnClickListener, FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener{

    Button botonFragUno;
    Button botonFragDos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);
        botonFragUno = (Button) findViewById(R.id.btnFrgUno);
        botonFragDos = (Button) findViewById(R.id.btnFrgDos);
        botonFragUno.setOnClickListener(this);
        botonFragDos.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.btnFrgUno:
                FragUno fragmenrtoUno = new FragUno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor, fragmenrtoUno);
                transactionUno.commit();
                break;
            case R.id.btnFrgDos:
                FragDos fragmenrtoDos = new FragDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor, fragmenrtoDos);
                transactionDos.commit();
                break;

        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public boolean onCreateOptionsMenu (Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(ActividadPrincipal.this);
                dialogoLogin.setContentView(R.layout.dlg_login);

                Button botonAutenticar = (Button) dialogoLogin.findViewById(R.id.btnAutentificar);
                final EditText cajaUsiario = (EditText) dialogoLogin.findViewById(R.id.txtUser);
                final EditText cajaClave = (EditText) dialogoLogin.findViewById(R.id.txtPassword);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ActividadPrincipal.this, cajaUsiario.getText().toString() + " " + cajaClave.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                });
                dialogoLogin.show();
                break;
            case R.id.opcionRegistrar:
                break;
        }
        return true;
    }
}
